############## Ko9n ##############
#    "3D" ecosystem simulator    #
#####(pronounced as konijnen)#####

*https://gitlab.com/mocchapi/ko9n*
*Anne Mocha (@mocchapi) 03/03/2021*
*for license, see LICENSE file*

----------------------------------

This is a simple ecosystem simulator
Animals behave in accordance to their
behavioral group, which determines
what action to follow when events occur

The world supports multiple biomes,
with many different tiles.
Both biomes & tiles can be added easily
biomes & height are determined
through layered opensimplex noise.

----------------------------------

To run you must have python 3.6+
installed along with the following
python packages:

opensimplex
ursina

you can install them with
`pip install -r requirements.txt`
if requirements.txt has more entries,
assume that list is correct

after installing, simply run `main.py`
using python as follows:
`python main.py`

please note that `python` might
default to python 2.7 on some systems
make sure to check the version if
it errors. Linux distros often use
`python3` instead of `python`

----------------------------------

This is being developed on an Manjaro
install, so no testing on windows.
If you get an error message on windows,
please open an issue on the gitlab page
https://gitlab.com/mocchapi/ko9n/-/issues
