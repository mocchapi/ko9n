import math
import time
import random
import threading

import ursina as u
from opensimplex import OpenSimplex
# from perlin import Perlin

# print(dir(u.color))
SPRITES = {}
SPRITES['fox'] = './assets/foxms.png'
SPRITES['rabbit'] = './assets/rabbitms.png'
SPRITES['tree1'] = './assets/tree1ms.png'
SPRITES['tree2'] = './assets/tree2ms.png'
SPRITES['flower1'] = './assets/flower1ms.png'
SPRITES['flower2'] = './assets/flower2ms.png'
SPRITES['wave'] = './assets/wavems.png'
SPRITES['palm1'] = './assets/palm1ms.png'
SPRITES['shell1'] = './assets/shell1ms.png'
SPRITES['dead'] = './assets/skullms.png'
SPRITES['snowman'] = './assets/snowmanms.png'
SPRITES['snowflake'] = './assets/snowflakems.png'
SPRITES['cactus'] = './assets/cactusms.png'
SPRITES['plant1'] = './assets/plant1ms.png'
SPRITES['plant2'] = './assets/plant2ms.png'
SPRITES['ice'] = './assets/icecubems.png'
SPRITES['bone'] = './assets/bonems.png'

class event():
    class BaseEvent():
        def __init__(self,name,originator,forgettime=-1,target=None,posX=None,posY=None):
            self.name = name
            self.originator = originator
            self.forgettime = forgettime
            self.target = target
            self.posX = posX
            self.posY = posY

    class HostileNearby(BaseEvent):
        def __init__(self,originator):
            super().__init__('HostileNearby',originator)

    class Hungry(BaseEvent):
        def __init__(self,originator):
            super().__init__('Hungry',originator)

    class Thirsty(BaseEvent):
        def __init__(self,originator):
            super().__init__('Thirsty',originator)

    class TargetedByHostile(BaseEvent):
        def __init__(self,originator,target):
            super().__init__('TargetedByHostile',originator,target=target)


class task():
    class BaseTask():
        def __init__(self,name,target,posX,posY,arguments=None):
            self.name = name
            self.target = target
            self.posX = posX
            self.posY = posY
            self.arguments = arguments
            if arguments == None:
                self.arguments = [self.target,self.posX,self.posY]
    class AbsoluteMoveTask(BaseTask):
        def __init__(self,*args,**kwargs):
            super().__init__('AbsoluteMoveTask',*args,**kwargs)
    class RelativeMoveTask(BaseTask):
        def __init__(self,*args,**kwargs):
            super().__init__('RelativeMoveTask',*args,**kwargs)
    class TowardsMoveTask(BaseTask):
        def __init__(self,steps,target,posX,posY):
            self.steps = steps
            super().__init__('TowardsMoveTask',target,posX,posY,arguments=[target,posX,posY,steps])
    class AwayMoveTask(BaseTask):
        def __init__(self,*args,**kwargs):
            super().__init__('AwayMoveTask',*args,**kwargs)


class tilecontent():
    class grass(): color = u.color.rgb(8,199,21)
    class snowgrass(): color = u.color.rgb(237, 255, 231)
    class lushgrass(): color = u.color.lime
    class drygrass(): color = u.color.rgb(194, 190, 0)
    class wetgrass(): color = u.color.rgb(0, 202, 148)
    class freshwater(): color = u.color.rgb(6,122,199)
    class saltwater(): color = u.color.rgb(0, 55, 219)
    class shallowsaltwater(): color = u.color.rgb(30, 85, 244)
    class clay(): color = u.color.rgb(120,120,120)
    class sand(): color = u.color.rgb(238, 252, 8)
    class dirt(): color = u.color.rgb(150, 111, 76)
    class snow(): color = u.color.rgb(223, 242, 251)
    class ice(): color = u.color.rgb(153, 228, 244)
    class stone(): color = u.color.rgb(91, 91, 91)
            # contents = [tilecontent.drygrass,tilecontent.sand]
            #
            # contents = [tilecontent.water,tilecontent.dirt]
            #
            # contents = [tilecontent.wetgrass]#,tilecontent.sand]


class tile():
    class Tile(u.Entity):
        def __init__(self,world,name,contents,posX,posY,height=0,color=None,foliage=[],scale=0.97,model='quad',biome=None):
            self.biome = biome
            self.height = -height
            self.foliage = foliage
            self.realfoliage = []
            self.world = world
            self.name = name
            self.posX = posX
            self.posY = posY
            self.actors = []
            # print(name,-1*height)
            if type(contents) in (list,tuple):
                self.contents = contents
            else:
                self.contents = [contents]
            if color == None:
                if self.contents:
                    color = self.contents[0].color
                else:
                    color = u.color.rgb(199,0,186)
            if self.foliage:
                if random.choice([True,False,False,False]):
                    self.SpawnFoliage(random.randint(1,3))
            super().__init__(model=model,color=color, position = u.Vec3(self.posX,self.posY,self.height), scale = 0.01, ignore=True, add_to_world_entities=False, collider='box')
            # super().__init__(model='cube',color=color, position = u.Vec3(self.posX,self.posY,self.height/2), scale = u.Vec3(0.9,0.9,1-self.height), ignore=True, add_to_world_entities=False)
            # self.animate_position()
            self.animate_scale(scale,duration=0.3, curve=u.curve.out_sine)
                        #            super().__init__(texture=texture,scale = 0.3,billboard=True,collider='box',**kwargs)
        def SpawnFoliage(self,amount):
            for i in range(0,amount):
                pos = u.Vec3(random.uniform(self.posX-0.4,self.posX+0.4),random.uniform(self.posY-0.4,self.posY+0.4),self.height+0.01)
                # print(pos,'-',self.posX,self.posY,self.height)
                a = u.Sprite(texture=random.choice(self.foliage),scale=0.11,billboard=True,position=pos)
                self.realfoliage.append(a)

        def PlaceActor(self,actor):
            # self.ignore = False
            tickratio = self.world.tickratio
            diff = abs((actor.posX+actor.posY)-(self.posX+self.posY))
            if diff == 0:
                diff = 1
            elif diff > 4:
                diff = 4
            duration = random.uniform(0.5/diff,0.8/diff)
            duration = duration*(tickratio/100)

            actor.animate_position(self.MakeActorVec(),duration=duration, curve=u.curve.linear)
            actor.posX = self.posX
            actor.posY = self.posY
            self.actors.append(actor)
            # actor.x = self.posX
            # actor.y = self.posX
        def MakeActorVec(self):
            newx = random.uniform(self.posX-0.4,self.posX+0.4)
            newy = random.uniform(self.posY-0.4,self.posY+0.4)
            newz = self.height+0.01
            return u.Vec3(newx,newy,newz)

        def UnplaceActor(self,actor):
            self.actors.remove(actor)
            # if len(self.actors) == 0:
            #     self.ignore = True

    class Rock(Tile):
        def __init__(self,world,posX,posY,height=0,biome=None):
            contents = [tilecontent.stone]
            foliage = []
            super().__init__(world,'rock',contents,posX,posY,height=height,foliage=foliage,biome=biome)

    class Grass(Tile):
        def __init__(self,world,posX,posY,height=0,biome=None):
            contents = [tilecontent.grass]
            foliage = (SPRITES['tree1'],SPRITES['tree2'])
            super().__init__(world,'grass',contents,posX,posY,height=height,foliage=foliage,biome=biome)

    class Lake(Tile):
        def __init__(self,world,posX,posY,height=0,biome=None):
            contents = [tilecontent.freshwater]
            height = height-0.15
            #foliage = #(SPRITES['wave'],)
            super().__init__(world,'lake',contents,posX,posY,height=height,biome=biome)

    class Ocean(Tile):
        def __init__(self,world,posX,posY,height=0,biome=None):
            contents = [tilecontent.saltwater]
            height = height-0.15
            foliage = (SPRITES['wave'],)
            super().__init__(world,'ocean',contents,posX,posY,height=height,foliage=foliage,biome=biome)

    class ShallowOcean(Tile):
        def __init__(self,world,posX,posY,height=0,biome=None):
            contents = [tilecontent.shallowsaltwater]
            height = height-0.15
            foliage = (SPRITES['wave'],)
            super().__init__(world,'shallowocean',contents,posX,posY,height=height,foliage=foliage,biome=biome)

    class Beach(Tile):
        def __init__(self,world,posX,posY,height=0,biome=None):
            contents = [tilecontent.sand,]
            foliage = (SPRITES['palm1'],SPRITES['shell1'])
            super().__init__(world,'beach',contents,posX,posY,height=height,foliage=foliage,biome=biome)

    class Desert(Tile):
        def __init__(self,world,posX,posY,height=0,biome=None):
            contents = [tilecontent.sand,]
            foliage = (SPRITES['cactus'],SPRITES['bone'])
            super().__init__(world,'beach',contents,posX,posY,height=height,foliage=foliage,biome=biome)

    class SnowyGrass(Tile):
        def __init__(self,world,posX,posY,height=0,biome=None):
            contents = [tilecontent.snowgrass,tilecontent.snow]
            foliage = (SPRITES['snowman'],SPRITES['tree2'])
            super().__init__(world,'snowygrass',contents,posX,posY,height=height,foliage=foliage,biome=biome)

    class JungleGrass(Tile):
        def __init__(self,world,posX,posY,height=0,biome=None):
            contents = [tilecontent.lushgrass,tilecontent.sand]
            foliage = []# (SPRITES['snowman'],SPRITES['tree2'])
            super().__init__(world,'junglegrass',contents,posX,posY,height=height,foliage=foliage,biome=biome)

    class DryGrass(Tile):
        def __init__(self,world,posX,posY,height=0,biome=None):
            contents = [tilecontent.drygrass,tilecontent.sand]
            foliage = []# (SPRITES['snowman'],SPRITES['tree2'])
            super().__init__(world,'drygrass',contents,posX,posY,height=height,foliage=foliage,biome=biome)

    class DirtyWater(Tile):
        def __init__(self,world,posX,posY,height=0,biome=None):
            contents = [tilecontent.freshwater,tilecontent.dirt]
            foliage = []# (SPRITES['snowman'],SPRITES['tree2'])
            super().__init__(world,'dirtywater',contents,posX,posY,height=height,foliage=foliage,biome=biome)

    class SwampGrass(Tile):
        def __init__(self,world,posX,posY,height=0,biome=None):
            contents = [tilecontent.wetgrass]#,tilecontent.sand]
            foliage = []# (SPRITES['snowman'],SPRITES['tree2'])
            super().__init__(world,'swampgrass',contents,posX,posY,height=height,foliage=foliage,biome=biome)


    class FrozenLake(Tile):
        def __init__(self,world,posX,posY,height=0,biome=None):
            contents = [tilecontent.ice]
            foliage = (SPRITES['snowflake'],)
            super().__init__(world,'frozenlake',contents,posX,posY,height=height,foliage=foliage,biome=biome)

class behavior():
    class BehaviorGroup():
        def __init__(self,default,responseDict,parent=None,ignorePriority=False):
            self.default = default
            self.parent = parent
            if self.parent != None:
                self.parent.behaviorGroup = self
            self.current = self.default
            self.responseDict = responseDict
            self.ignorePriority = ignorePriority

        def SetParent(self,parent):
            self.parent = parent

        def Do(self):
            print(self.parent.beebugname,'running behavior',self.current.name)
            done = self.current.Do(self.parent)
            if done:
                if self.current != self.default: print(self.parent.beebugname,'switching to default',self.default.name,'due to',self.current.name,'finishing')
                self.current = self.default

        def Event(self,event):
            newbehavior = self.responseDict.get(type(event),self.default)
            if newbehavior == self.default:
                print(self.parent.beebugname,'received event',event.name,'but had no corresponding behavior')
            if self.current.priority < newbehavior.priority:
                print(self.parent.beebugname,'switching to',newbehavior.name,'due to event',event.name)
                # print(self.parent.debugname,'running EVENT behavior',self.current.name)
                # self.current.SaveEvent(event)
                self.current = newbehavior
            self.Do()


    class BaseBehavior():
        def __init__(self,name,priority):
            self.name = name
            self.priority = priority

        def Do(self,parent):
            print(f'Behavior {self.name} belonging to {parent.name} is not implemented')

    class Wander(BaseBehavior):
        def __init__(self,priority,tiles=(tile.Grass,tile.Beach,tile.SnowyGrass,tile.FrozenLake),speed=1,generate=True):
            self.tiles = tiles
            self.generate = generate
            self.speed = speed
            super().__init__('Wander',priority)
        def Do(self,parent):
            if self.speed > parent.stats['maxspeed']:
                speed = parent.stats['maxspeed']
            else:
                speed = self.speed
            if speed > 0:
                potentials = parent.GetNearbyTiles(radius=speed,generate=self.generate)
                reals = []
                for tile in potentials:
                    if type(tile) in self.tiles:
                        reals.append(tile)
                if len(reals) > 0:
                    print('Tiles in reals:',reals)
                    final = random.choice(reals)
                    print('Chosen tile:',final.name)
                    parent.MoveTowards(final.posX,final.posY,speed)
                else:
                    print('No tiles in reals! using BEANS')
                    theBEANS = (-1,0,0,0,0,0,1)
                    newx = random.choice(theBEANS)
                    newy = random.choice(theBEANS)
                    parent.MoveRelative(newx,newy)
            return True

    class FindTileAndDoAction(BaseBehavior):
        def __init__(self,name,priority,target,action,speed):
            self.speed = speed
            self.target = target
            self.targettile = None
            self.action = action
            super().__init__(name,priority)

        def Do(self,parent):
            if self.speed > parent.stats['maxspeed']:
                speed = parent.stats['maxspeed']
            else:
                speed = self.speed
            currenttile = parent.GetTile()
            if self.target == type(currenttile):
                print(parent.beebugname,self.name,'at target',self.target)
                return self.action(parent)
                # if parent.stats['hydration'] > parent.stats['thirstylevel']:
                #     return random.choice([True,False,False])
            elif self.targettile:
                print(parent.beebugname,self.name,'moving towards',self.targettile.posX,self.targettile.posY,'from',parent.posX,parent.posY)
                tile = self.targettile
                parent.MoveTowards(tile.posX,tile.posY,self.speed)
            else:
                print(parent.beebugname,self.name,'looking for',self.target)
                for tile in parent.GetNearbyTiles():
                    if self.target == type(tile):
                        self.targettile = tile
                        parent.MoveTowards(tile.posX,tile.posY,self.speed)
                        print(parent.beebugname,self.name,'found',self.targettile.name)
                        return False
                print(parent.beebugname,self.name,'no',self.target,'found, wandering')
                a = behavior.Wander(0,tiles=(type(self.target),),speed=self.speed,generate=False)
                a.Do(parent)

    class FindWaterAndDrink(FindTileAndDoAction):
        def __init__(self,priority):
                super().__init__('FindWaterAndDrink',priority,tile.Lake,self.doaction,99)
        def doaction(self,parent):
            parent.Drink()
            if parent.stats['hydration'] > parent.stats['thirstylevel']:
                return random.choice([True,False,False])
            # self.targettile = None
        #     super().__init__('findanddrink',priority)
        # def Do(self,parent):
        #     currenttile = parent.GetTile()
        #     if tilecontent.freshwater in currenttile.contents:
        #         parent.Drink()
        #         if parent.stats['hydration'] > parent.stats['thirstylevel']:
        #             return random.choice([True,False,False])
        #     elif self.targettile:
        #         tile = self.targettile
        #         parent.MoveTowards(tile.posX,tile.posY,parent.stats['maxspeed'])
        #     else:
        #         for tile in parent.GetNearbyTiles():
        #             if tilecontent.freshwater in tile.contents:
        #                 self.targettile = tile
        #                 parent.MoveTowards(tile.posX,tile.posY,parent.stats['maxspeed'])
        #                 break:+=
    class FindGrassAndEat(FindTileAndDoAction):
        def __init__(self,priority):
            super().__init__('FindGrassAndEat',priority,tile.Grass,self.doaction,99)
        def doaction(self,parent):
            parent.stats['food'] += parent.stats['maxfood']/9
            print(parent.beebugname,'ate, food now',parent.stats['food'])
            if parent.stats['food'] > parent.stats['hungrylevel']:
                return random.choice([True,False,False])
    # class FindAndEatGrass(BaseBehavior):
    #     def __init__(self,priority):
    #         self.targettile = None
    #         super().__init__('FindAndEatGrass',priority)
    #     def Do(self,parent):
    #         currenttile = parent.GetTile()
    #         if tilecontent.grass in currenttile.contents:
    #             parent.stats['food'] += 10
    #             print(parent.beebugname,'ate, food now',parent.stats['food'])
    #             if parent.stats['food'] > parent.stats['hungrylevel']:
    #                 return random.choice([True,False,False])
    #         elif self.targettile:
    #             tile = self.targettile
    #             parent.MoveTowards(tile.posX,tile.posY,parent.stats['maxspeed'])
    #         else:
    #             for tile in parent.GetNearbyTiles():
    #                 if tilecontent.grass in tile.contents:
    #                     self.targettile = tile
    #                     parent.MoveTowards(tile.posX,tile.posY,parent.stats['maxspeed'])
    #                     break

class actor():
    class BaseActor(u.Sprite):
        def __init__(self,name,world,stats,behaviorGroup,posX,posY,texture,nickname=None,statVariance=0.02,**kwargs):
            super().__init__(texture=texture,scale = 0.15,billboard=True,collider='box',**kwargs)

            self.statVariance = statVariance
            self.world = world
            self.realname = name
            if nickname:
                self.name = nickname
            else:
                self.name = self.realname
            self.alive = True
            self.behaviorGroup = behaviorGroup
            self.livedticks = 0
            self.id = self.world.GetUnique()
            self.beebugname = f'{self.realname}-{self.name}#{self.id}'

            self.posX = posX
            self.posY = posY

            self.stats = {'strenght':5,'eyesight':2,'searchsight':4,'maxhealth':100,'health':100,'hydration':100,'dehydrationspeed':0.8,'maxhydration':100,'food':100,'starvationspeed':0.3,'hungrylevel':35,'thirstylevel':35,'maxfood':100,'maxspeed':2,'defense':3}
            self.stats = {**self.stats,**stats}
            if self.statVariance > 0:
                for item in stats:
                    val = self.stats[item]
                    self.stats[item] = round(random.uniform(val-val*self.statVariance,val+val*self.statVariance),2)
            self.eventqueue = []
            self.behaviorGroup.SetParent(self)
            self.world.PlaceActor(self,posX,posY)
            # if self.showname:
            #     self.nametextobj = u.Text(text=self.beebugname,scale=self.scale)
            #     self.nametextobj.x = self.x
            #     self.nametextobj.y = self.y
            # else:
            #     self.ignore = True

        # def update(self):
        #     if self.showname:
        #         self.nametextobj.x = self.x
        #         self.nametextobj.y = self.y

        def GetPos(self):
            return self.world.GetEntityPos(self)

        def Drink(self):
            if tilecontent.freshwater in self.GetTile().contents:
                self.stats['hydration'] += int(self.stats['maxhydration']/6)
                print(self.beebugname,'drank, hydration now',self.stats['hydration'])
                if self.stats['hydration'] > self.stats['maxhydration']:
                    self.stats['hydration'] = self.stats['maxhydration']
                # print(f'({self.debugname}) drank, hydration now {self.stats['hydration']}')

        def GetTile(self):
            return self.world.GetActorPos(self)

        def GetNearbyActors(self,radius=None,generate=True):
            if radius == None:
                radius = self.stats['searchsight']
            return self.world.GetNearbyActors(self,radius=radius,generate=generate)
        def GetNearbyTiles(self,radius=None,generate=True):
            if radius == None:
                radius = self.stats['searchsight']
            return self.world.GetNearbyTiles(self,radius=radius,generate=generate)

        def Teleport(self,posX,posY):
            self.world.QueueTask(task.AbsoluteMoveTask(self,posX,posY))
            # self.world.MoveActorAbsolute(self,posX,posY)

        def MoveRelative(self,deltaX,deltaY):
            self.world.QueueTask(task.RelativeMoveTask(self,deltaX,deltaY))
            # self.world.MoveActorRelative(self,deltaX,deltaY)
        def MoveTowards(self,posX,posY,steps):
            if steps > self.stats['maxspeed']:
                steps = self.stats['maxspeed']
            self.world.QueueTask(task.TowardsMoveTask(steps,self,posX,posY))

        def Damage(self,amount,ignoredef=False):
            if not ignoredef:
                realdamage = amount - int(1.5*defense)
            else:
                realdamage = amount
            if realdamage < 0:
                realdamage = 0
            self.stats['health'] = round(self.stats['health']-realdamage,2)
            if self.stats['health'] <= 0:
                self.Kill()

        def Kill(self):
            self.stats['health'] == 0
            print(self.beebugname,'is ded :(')
            # self.fade_out()
            # self.disable()
            self.texture = SPRITES['dead']
            self.alive = False

        def Update(self,ticks):
            if self.alive:
                # self.world.Log(self.nickname,'Running update').Unplace
                if self.stats['health'] <= 0:
                    self.Kill()
                self.stats['hydration'] = round(self.stats['hydration'] - self.stats['dehydrationspeed'],2)
                self.stats['food'] = round(self.stats['food'] - self.stats['starvationspeed'],2)
                if self.stats['hydration'] <= self.stats['thirstylevel']:
                    self.ReceiveEvent(event.Thirsty(self))
                if self.stats['food'] <= self.stats['hungrylevel']:
                    self.ReceiveEvent(event.Hungry(self))
                if self.stats['hydration'] <= 0:
                    self.Damage(self.stats['maxhealth']*0.05,ignoredef=True)
                if self.stats['food'] <= 0:
                    self.Damage(self.stats['maxhealth']*0.03,ignoredef=True)

                if len(self.eventqueue) != 0:
                    # print(f'!!! {self.debugname} EVENTS: {self.eventqueue}')
                    self.behaviorGroup.Event(self.eventqueue[0])
                    del self.eventqueue[0]
                else:
                    self.behaviorGroup.Do()

        def QueueTask(self,task):
            self.world.QueueTask(self,task)

        def ReceiveEvent(self,event):
            # print(self.name,'received event',event.name)
            self.eventqueue.append(event)

    class Rabbit(BaseActor):
        def __init__(self,world,posX,posY,nickname=None):
            stats = {'eyesight':2}
            behaviorGroup = behavior.BehaviorGroup(behavior.Wander(0),{event.Thirsty:behavior.FindWaterAndDrink(9),event.Hungry:behavior.FindGrassAndEat(9)})
            super().__init__('Rabbit',world,stats,behaviorGroup,posX,posY,SPRITES['rabbit'],nickname=nickname)

    class Fox(BaseActor):
        def __init__(self,world,posX,posY,nickname=None):
            stats = {'eyesight':3}
            behaviorGroup = behavior.BehaviorGroup(behavior.Wander(0),{event.Thirsty:behavior.FindWaterAndDrink(9),event.Hungry:behavior.FindGrassAndEat(9)})
            super().__init__('Fox',world,stats,behaviorGroup,posX,posY,SPRITES['fox'],nickname=nickname)

def GetClosest(number,collection):
    out = min(collection,key=lambda x:abs(float(x)-float(number)))
    # print('closest to',number,'out of',collection,'is',out)
    return out

class biome():
    class val():
        MIN = -1
        LOW = -0.5
        MID = 0
        HIGH = 0.5
        MAX = 1
        ALL = [MIN,LOW,MID,HIGH,MAX]
    class BaseBiome():
        def __init__(self, name, properties, gendict, debugcolor, mapmode = False):
            self.properties = properties
            self.name = name
            self.mapmode = mapmode
            self.debugcolor = debugcolor
            # self.constant = constant
            self.gendict = gendict
        def Generate(self, x, y, z, world):
            # z = noisemaker.noise2d(x*self.constant,y*self.constant)
            # if self.mapmode:
                #         def __init__(self,world,name,contents,posX,posY,height=0,color=None,foliage=[]):
                # return tile.Tile(world,'debugmap',[],x,y,color=self.debugcolor,height=-1.5)
            tilenum = GetClosest(z, self.gendict.keys())
            newtile = self.gendict[tilenum]
            print('[TERRAINGEN]',self.name,x,y,z,newtile)
            return self.AssembleTile(x, y, z, world, newtile)
        def AssembleTile(self, x, y, z, world, newtile):
            return newtile(world,x,y,z,biome=self.name)

    class RockySpikes(BaseBiome):
        def __init__(self,*args,**kwargs):
            gendict = {0.5:tile.Ocean,0.7:tile.Ocean,0.71:tile.Rock}
            properties = {'humidity': biome.val.HIGH, 'temperature':biome.val.MID}
            super().__init__('RockySpikes', properties, gendict, u.color.gray, *args, **kwargs)
        def AssembleTile(self, x, y, z, world, newtile):
            if newtile == tile.Rock:
                return newtile(world,x,y,z,biome=self.name)
            else:
                return newtile(world,x,y,-abs(1+z*0.3),biome=self.name)

    class SubnauticOcean(BaseBiome):
        def __init__(self,*args,**kwargs):
            gendict = {-0.4:tile.Ocean, 0:tile.ShallowOcean}
            properties = {'humidity': biome.val.HIGH, 'temperature':biome.val.LOW}
            super().__init__('SubnauticOcean', properties, gendict, u.color.azure, *args, **kwargs)
        def AssembleTile(self, x, y, z, world, newtile):
            return newtile(world,x,y,-abs(1+z*0.3),biome=self.name)

    class DustyDesert(BaseBiome):
        def __init__(self,*args,**kwargs):
            gendict = {0.3:tile.Desert}
            properties = {'humidity': biome.val.MIN, 'temperature':biome.val.MAX}
            super().__init__('DustyDesert', properties, gendict, u.color.salmon, *args, **kwargs)
        def AssembleTile(self, x, y, z, world, newtile):
            return newtile(world,x,y,z*1.5,biome=self.name)

    class ForestIsland(BaseBiome):
        def __init__(self, *args, waterlevel=-0.4, beachlevel=0, **kwargs):
            properties = {'humidity': biome.val.MID, 'temperature':biome.val.MID}
            self.waterlevel = waterlevel
            self.beachlevel = beachlevel
            gendict = self.gendict = {self.waterlevel:tile.Lake, self.beachlevel:tile.Beach, self.beachlevel+.1:tile.Grass}
            super().__init__('ForestIsland', properties, gendict, u.color.green, *args, **kwargs)
        def AssembleTile(self, x, y, z, world, newtile):
            return newtile(world,x,y,z+self.waterlevel,biome=self.name)

    class ColdHillValleys(BaseBiome):
        def __init__(self,*args,**kwargs):
            gendict = {-0.7:tile.Rock,-0.69:tile.SnowyGrass,0.3:tile.SnowyGrass, 0.31:tile.FrozenLake}
            properties = {'humidity': biome.val.MID, 'temperature':biome.val.LOW}
            super().__init__('ColdHillValleys', properties, gendict, u.color.white, *args, **kwargs)
        def AssembleTile(self, x, y, z, world, newtile):
            return newtile(world,x,y,z*-1,biome=self.name)

    class JankyJungle(BaseBiome):
        def __init__(self,*args,**kwargs):
            gendict = {0:tile.JungleGrass}
            properties = {'humidity':biome.val.HIGH,'temperature':biome.val.HIGH}
            super().__init__('JankyJungle', properties, gendict, u.color.lime)

    # class TrickyTundra(BaseBiome):
    #     def __init__(self,*args,**kwargs):
    #         gendict = {0:tile.TundraGrass}
    #         properties = {'humidity':biome.val.,'temperature':biome.val.LOW}
    #         super().__init__('TrickyTundra', properties, gendict, u.color.lime)

    class StrangeSteppe(BaseBiome):
        def __init__(self,*args,**kwargs):
            gendict = {0:tile.DryGrass}
            properties = {'humidity':biome.val.LOW,'temperature':biome.val.MID}
            super().__init__('StrangeSteppe', properties, gendict, u.color.lime)

    class ShittySwamps(BaseBiome):
        def __init__(self,*args,**kwargs):
            gendict = {-0.2:tile.DirtyWater, 0:tile.SwampGrass}
            properties = {'humidity':biome.val.HIGH,'temperature':biome.val.MID}
            super().__init__('TrickyTundra', properties, gendict, u.color.lime)

class BasicTerrainGen():
    def __init__(self,seed=1,constants={},waterlevel=0,splitconst=0.08,mapmode=''):
        if seed == 0:
            seed = 5
        self.noisetracker = 0
        seed = abs(seed)
        self.noisemaker = OpenSimplex(seed=seed)
        self.waterlevel = waterlevel
        self.mapmode = mapmode
        if 'biome' in self.mapmode:
            biomemode = True
        else:
            biomemode = False
        self.const = {**{'height':0.075, 'humidity':0.025, 'temperature':0.025}, **constants}
        self.splitconst = splitconst
        self.intermitter = biome.SubnauticOcean()
        biomes = [biome.ForestIsland(mapmode=biomemode), biome.ShittySwamps(mapmode=biomemode), biome.StrangeSteppe(mapmode=biomemode), biome.DustyDesert(mapmode=biomemode), biome.ColdHillValleys(mapmode=biomemode), biome.JankyJungle(mapmode=biomemode)]
        self.biomes = {}
        for item in biomes:
            if not self.biomes.get(item.properties['humidity']):
                self.biomes[item.properties['humidity']] = {}
            # if not self.biomes[item.properties['humidity']].get(item.properties['temperature']):
            self.biomes[item.properties['humidity']][item.properties['temperature']] = item
        # print(self.biomes)
        for item in self.biomes:
            print('humidity:',item)
            for a in self.biomes[item]:
                print('  temp:',a,self.biomes[item][a])

        # self.biomes = {'1001':biome.ForestIsland(mapmode=self.biomemap),'1111':self.intermitter,'1110':biome.DustyDesert(mapmode=self.biomemap),'0001':biome.ColdHillValleys(mapmode=self.biomemap)}

    def ChangeSeed(self,newseed):
        self.noisemaker = OpenSimplex(seed=newseed)

    def GetBiome(self,x,y):
        # spice =   self.noisemaker.noise3d(x*self.const['height']*1, y*self.const['height']*1, 1)*0.5
        # spice +=  self.noisemaker.noise3d(x*self.const['height']*2, y*self.const['height']*2, 2)*0.25
        # spice +=  self.noisemaker.noise3d(x*self.const['height']*4, y*self.const['height']*4, 3)*0.125
        # spice = 0
        humidity = self.noisemaker.noise3d(x*self.const['humidity'],y*self.const['humidity'],4) + self.GetSpice(x,y,self.const['humidity'])
        temperature = self.noisemaker.noise3d(x*self.const['temperature'],y*self.const['temperature'],5) + self.GetSpice(x,y,self.const['temperature'])
        a = GetClosest(humidity,self.biomes.keys())
        print()
        print('  humidity:',humidity,'->',a,list(self.biomes.keys()))
        b = GetClosest(temperature,self.biomes[a].keys())
        print('  temp:',temperature,'->',b,list(self.biomes[a].keys()))
        print(f'  [{a}][{b}]')
        biome = self.biomes[a][b]
        print('  chosen:',biome.name)
        return biome


    def GetSpice(self,x,y,const):
        spice =   self.noisemaker.noise3d(x*const*0.125, y*const*0.125, 1)*0.5
        spice +=  self.noisemaker.noise3d(x*const*0.25, y*const*0.25, 2)*0.25
        spice +=  self.noisemaker.noise3d(x*const*0.5, y*const*0.5, 3)*0.125
        return spice


    def Generate(self,world,x,y):
        z = self.noisemaker.noise2d(x*self.const['height'],y*self.const['height']) + self.GetSpice(x,y,self.const['height'])
        if self.mapmode:
            r = 0
            g = 0
            b = 0
            h = z+1
            if 'temperature' in self.mapmode:
                z1 = self.noisemaker.noise3d(x*self.const['temperature'],y*self.const['temperature'],5) + self.GetSpice(x,y,self.const['temperature'])
                r = 255*((GetClosest(z1,biome.val.ALL)+1)/2)
                if 'overlay' in self.mapmode: tile.Tile(world,'mapmode',[],x+0.1,y,height=h,color=u.color.rgb(r,0,0),scale=0.4,model='circle',biome=str(GetClosest(z1,biome.val.ALL)))
            if 'humidity' in self.mapmode:
                z1 = self.noisemaker.noise3d(x*self.const['humidity'],y*self.const['humidity'],4) + self.GetSpice(x,y,self.const['humidity'])
                b = 255*((GetClosest(z1,biome.val.ALL)+1)/2)
                if 'overlay' in self.mapmode: tile.Tile(world,'mapmode',[],x-0.1,y,height=h-0.1,color=u.color.rgb(0,0,b),scale=0.4,model='circle',biome=str(GetClosest(z1,biome.val.ALL)))
            if 'humidity' in self.mapmode and 'temperature' in self.mapmode and 'overlay' in self.mapmode:
                tile.Tile(world,'mapmode',[],x,y-0.1,height=h+0.1,color=u.color.rgb(r,0,b),scale=0.4,model='circle')
            if 'water' in self.mapmode:
                if z <= self.waterlevel:
                    h -= 1
                    r *= 0.35
                    b *= 0.35
                    # g = 255*0.8
            clr = u.color.rgb(r,g,b)
            if 'overlay' in self.mapmode:
                pass
                # return tile.Tile(world,'mapmode',[],x,y,height=h,color=clr,scale=0.6,model='circle')
            else:
                return tile.Tile(world,'mapmode',[],x,y,height=h,color=clr,biome='debug')

            # a.x += 0.5
            # a.y += 0.5
        if z > self.waterlevel:
            newbiome = self.GetBiome(x,y)
            return newbiome.Generate(x,y,z-(abs(self.waterlevel+1)/2),world)
        else:
            return self.intermitter.Generate(x,y,z,world)


class World(u.Entity):
    def __init__(self,maxX=0,maxY=0,terraingen=BasicTerrainGen(),pregenerate=False,tickratio=100,paused=False):
        self.paused = paused
        self.maxX = maxX
        self.maxY = maxY
        self.ids = 0
        self.chunkspace = {}
        self.terraingen = terraingen
        self.ticks = 0
        self.taskqueue = []
        self.tickratio = tickratio
        self.tickcounter = 0
        self.chunkspace = {}


                        # print('nyoom')
                # threading.Thread(target=RenderRow,args=(self,x)).start()
                # time.sleep(0.001)

        if pregenerate:
            threading.Thread(target=self.Pregenerate,args=(pregenerate[0],pregenerate[1]), daemon=True).start()
        super().__init__()

    def Pregenerate(self,maxx,maxy):
        print('Pregenerate go')
        for x in range(maxx):
            time.sleep(0.002)
            for y in range(maxy):
                self.GetWorldPos(x,y)
                time.sleep(0.002)

    def QueueTask(self,task):
        # print(f'[Q] Queueing task {task.name} from {task.target.debugname}')
        self.taskqueue.append(task)

    def RunTask(self,thistask):
        items = {task.AbsoluteMoveTask:self.MoveActorAbsolute,
        task.RelativeMoveTask:self.MoveActorRelative,
        task.TowardsMoveTask:self.MoveActorTowards,
        }
        if thistask.target.alive and (func:=items.get(type(thistask))):
            # print(f'[R] Running task {task.name} from {task.target.debugname}')
            func(*thistask.arguments)

    def Pause(self):
        self.paused = True
    def Unpause(self):
        self.paused = False
    def TogglePause(self):
        if self.paused:
            self.Unpause()
        else:
            self.Pause()

    def update(self):
        if not self.paused:
            if self.tickratio > 0:
                self.tickcounter += 1
                if self.tickcounter >= self.tickratio:
                    self.Update()
                    self.tickcounter = 0
            else:
                self.tickcounter = 0

    def Update(self):
        # self.Syslog('(World)','Running tick ',self.ticks)
        for chunkspaceY in list(self.chunkspace.values()):
            for tile in list(chunkspaceY.values()):
                for actor in tile.actors:
                    actor.Update(self.ticks)
        for task in self.taskqueue:
            self.RunTask(task)
        self.taskqueue = []
        self.ticks += 1

    def GetUnique(self):
        self.ids += 1
        return self.ids

    def GetWorldPos(self,x,y,generate=True):
        if not self.chunkspace.get(y):
            self.chunkspace[y] = {}
        if not self.chunkspace[y].get(x):
            if generate:
                self.chunkspace[y][x] = self.terraingen.Generate(self,x,y)#(self,x,y)
        return self.chunkspace[y].get(x)

    def PlaceActor(self,actor,x=0,y=0):
        self.GetWorldPos(x,y).PlaceActor(actor)

    def UnplaceActor(self,actor):
        self.GetActorPos(actor).UnplaceActor(actor)

    def MoveActorAbsolute(self,actor,posX,posY):
        # print('Moving actor',actor.beebugname,'from',actor.posX,actor.posY,'to',posX,posY)
        # if (0 <= posX <= self.lenX) and (0 <= posY <= self.lenY) and not (actor.posX,actor.posY) == (posX,posY):
        if  (actor.posX,actor.posY) != (posX,posY):
            self.UnplaceActor(actor)
            self.PlaceActor(actor,posX,posY)
            self.GetNearbyTiles(actor,sort=False,generate=True)
        else:
            duration = random.uniform(0.5,0.8)
            duration = duration*(self.tickratio/100)
            actor.animate_position(self.GetWorldPos(posX,posY).MakeActorVec(),duration=duration)

    def MoveActorRelative(self,actor,deltaX,deltaY):
        self.MoveActorAbsolute(actor,actor.posX+deltaX,actor.posY+deltaY)

    def MoveActorAway(self,actor,posX,posY,steps):
        deltaX = self.posX-posX
        deltaY = self.posY-posY
        if deltaY > 0:
            if deltaY > steps:
                deltaY = steps
        else:
            if deltaY < steps:
                deltaY = -1*steps
        if deltaX > 0:
            if deltaX > steps:
                deltaX = steps
        else:
            if deltaX < steps:
                deltaX = -1*steps


    def MoveActorTowards(self, actor, posX, posY, steps=1):
        if steps < 0:
            steps = steps*-1
        directionX = posX - actor.posX
        directionY = posY - actor.posY
        if directionX > 0:
            if directionX > steps:
                directionX = steps
        else:
            if abs(directionX) > steps:
                directionX = steps*-1

        if directionY > 0:
            if directionY > steps:
                directionY = steps
        else:
            if abs(directionY) > steps:
                directionY = steps*-1
        self.MoveActorRelative(actor,directionX,directionY)

    def GetActorPos(self,actor,offsetX=0,offsetY=0):
        return self.GetWorldPos(actor.posX+offsetX,actor.posY+offsetY)

    def GetNearbyTilesPos(self,posX,posY,radius=2,sort=True,generate=True,animate=False):
        out = []
        # print(range(posY-radius,posY+radius+1),range(posX-radius,posX+radius+1))
        if radius == 0:
            out = [self.GetWorldPos(posX,posY)]
        else:
            for y in range(int(posY-radius),int(posY+1+radius)):
                for x in range(int(posX-radius),int(posX+1+radius)):
                    thing = self.GetWorldPos(x,y,generate=generate)
                    if thing:
                        out.append(thing)
            if sort:
                out.sort(key=lambda x: (abs(x.posY-posY)+abs(x.posX-posX)))
                # print('origin:',posX,posY)
        if animate:
            for item in out:
                item.blink(value=u.color.white,duration=1,delay=0.07*(abs(item.posY-posY)+abs(item.posX-posX)))
                    # print(type(item),item)
                    # print('x:',item.posX,'y:',item.posY,'diff:', )
        return out

    def GetNearbyTiles(self,actor,radius=None,sort=True,generate=True):
        if radius == None:
            radius = actor.stats['eyesight']
        return self.GetNearbyTilesPos(actor.posX,actor.posY,radius=radius,sort=sort,generate=generate)

    def GetNearbyActors(self,actor,radius=2,generate=True):
        out = self.GetNearbyTiles(actor,radius=range,generate=generate)
        actors = []
        for tile in out:
            if tile:
                actors += tile.actors
        return actors

    def SetTickRatio(self,newtickratio):
        self.tickratio = newtickratio

class ActorDebugUI(u.Entity):
    def __init__(self,world):
        self.waitticks = 0
        self.world = world

        self.cam = u.EditorCamera()
        # specials = []
        # for item in dir(self.cam):
        #     print(item,type(item))
        #     if 'zoom' in str(item):
        #         specials.append(str(item))
        # print(specials)
        # print(dir(self.cam))
        self.selectedActor = None
        self.selectedTile = None

        self.positiontext = u.Text('(?,?)')
        self.hovertext = u.Text('Selected item: None')
        self.infotext = u.Text('?')
        self.itempostext = u.Text('x?, y?, z?')
        self.statsdict = {}
        self.statspanel = None
        # button_dict = {'Move cam':lambda: self.button('movecam'),'Clear selection':lambda:self.button('clearselection')}
        buttons = []
        buttons.append(u.Button("Clear selection",on_click=lambda: self.button('clearselection')))
        buttons.append(u.Button("Move cam",on_click=lambda: self.button('movecam')))
        buttons.append(u.Button("Toggle stats",on_click=lambda: self.button('togglestats')))
        self.panel = u.WindowPanel(title="Inspector",content=[self.positiontext,self.hovertext,self.infotext,self.itempostext]+buttons)
        # super().__init__(title="Inspector",centent=(self.positiontext,))
        self.panel.y = -0.11
        self.panel.x = -0.6
        super().__init__()

    def button(self,value):
        if value == 'togglestats':
            if self.statspanel:
                if self.statspanel.visible:
                    self.statspanel.visible = False
                else:
                    self.statspanel.visible = True
        elif value == 'clearselection':
            self.selectedActor = None
            self.selectedTile = None
            if self.statspanel:
                self.statspanel.enabled = False
                self.statspanel = None
                self.infotext.text = 'Actor behavior: None'

        elif value == 'movecam':
            if self.selectedActor:
                self.cam.animate_position(self.selectedActor.position,duration=0.5)
            elif self.selectedTile:
                self.cam.animate_position(self.selectedTile.position,duration=0.5)


    def changestat(self):
        if self.selectedActor and self.changestatWindow:
            self.selectedActor.stats[self.changestatKeyInput.text] = float(self.changestatValInput,text)

    def updatestats(self):
        out = {}
        for key in self.selectedActor.stats:
            text = self.statsdict.get(key)
            if not text:
                text = u.Text("")
                self.statsdict[key] = text
            text.text = f'{key}: {self.selectedActor.stats[key]}'
        if not self.statspanel:
            self.statspanel = u.WindowPanel(title='Actor stats',content=list(self.statsdict.values()))
            self.statspanel.visible = False

    def update(self):
        if self.waitticks:
            self.waitticks -= 1
        if u.held_keys['r'] and not self.waitticks:
            print('cam reset')
            self.cam.animate_position(u.Vec3(0,0,10))
            self.cam.animate_rotation(u.Vec3(0,0,0))
            u.camera.animate_position(u.Vec3(0,0,-15))
            u.camera.animate_rotation(u.Vec3(0,0,0))
            u.camera.fov = 90
            self.waitticks += 20
        self.positiontext.text = f'Mouse position: (x{round(u.mouse.x,3)}, y{round(u.mouse.y,3)})'
        hoveractor = u.mouse.hovered_entity
        if isinstance(hoveractor,actor.BaseActor) and u.mouse.left:
            self.selectedTile = None
            self.selectedActor = hoveractor
            self.hovertext.text = f'Selected item: {self.selectedActor.beebugname}'
        elif isinstance(hoveractor,tile.Tile) and u.mouse.left:
            self.selectedActor = None
            self.selectedTile = hoveractor
            try:
                self.hovertext.text = f'Selected tile: {self.selectedTile.name}'
                self.itempostext.text = f'x{self.selectedTile.posX}, y{self.selectedTile.posY}, z{self.selectedTile.height}'
                self.infotext.text = f'Tile biome: {self.selectedTile.biome}'
            except Exception as e:
                print(e)

        if self.selectedActor:
            self.updatestats()
            self.itempostext.text = f'x{round(self.selectedActor.x,2)}, y{round(self.selectedActor.y,2)}, z{round(self.selectedActor.z,2)}'
            self.infotext.text = f'Actor behavior: {self.selectedActor.behaviorGroup.current.name}'
        elif self.selectedTile:
            pass
        else:
            self.hovertext.text = 'Selected item: None'
            self.infotext.text = ''

        # u.invoke(u.mouse.unhover_everything_not_hit,delay=1)
        u.mouse.unhover_everything_not_hit()
        # ()
            # self.hovertext.text = f'Hovered object: {hoveractor.name}'

class DebugUI(u.Entity):
    def __init__(self,world):
        self.world = world
        self.a = u.Text(f'Tick count: {self.world.ticks}')
        b = u.Slider(min=0,default=self.world.tickratio,max=150,vertical=False,step=1)
        b.on_value_changed = lambda: self.world.SetTickRatio(b.value)
        c = u.Button(text='Single step',on_click=lambda: self.world.Update(),scale=.1)
        d = u.Button(text='Toggle pause',on_click=lambda: self.world.TogglePause(),scale=.1)
        self.pausedorno = u.Text(f'World state: ')
        self.panel = u.WindowPanel(title='World',content=(self.pausedorno,self.a,b,c,d))
        self.panel.y = 0.3
        self.panel.x = -0.6
        super().__init__()
    def update(self):
        self.a.text = f'Tick count: {self.world.ticks}'
        if self.world.paused:
            self.pausedorno.text = 'World state: Paused'
        else:
            self.pausedorno.text = 'World state: Running'

if __name__ == '__main__':
    app = u.Ursina()
    pregenerate = (60,60)
    # pregenerate = False
    seed = 605
    seed = random.randint(0,99999)
    mapmode = 'temperature & humidity & water & overlay'
    mapmode = ''
    # mapmode = ''
    worldgen = BasicTerrainGen(seed,mapmode=mapmode)
    earth = World(terraingen=worldgen,pregenerate=pregenerate,tickratio=100,paused=True)

    u.Text('seed: '+str(seed),x=0,y=0.48)
    print(seed)
    u.Text(text='temperature', x=0.8, y=0.45)
    for index,value in enumerate(biome.val.ALL):
        u.Text(text=f'{value}', x=0.8, y=0.4-0.05*index, color=u.color.rgb(255*((value)+1)/2,0,0))
    u.Text(text='humidity', x=0.65, y=0.45)
    for index,value in enumerate(biome.val.ALL):
        u.Text(text=f'{value}', x=0.65, y=0.4-0.05*index,color=u.color.rgb(0,0,255*((value)+1)/2))
    # for index,value in enumerate(biome.val.ALL):
    #     u.Text(text=f'{value}',starting_position=u.Vec2(0.4,0.4-+0.1*index),color=u.color.rgb(255*((value)+1)/2))
    # for i in range(0,5):
    #     actor.Fox(earth,15,15)
    for i in range(0,15):
        actor.Rabbit(earth,8,8)
    # unlucky = actor.Rabbit(earth,3,0,"unlucky")
    # unlucky.stats['health'] = 0
    print('hewoo')
    u.window.borderless = False
    a = DebugUI(earth)
    b = ActorDebugUI(earth)

    # print(dir(u.color))
    # print('---')
    # print(dir(u.color.salmon))
    app.run()
